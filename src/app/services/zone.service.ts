import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ZoneService {
  constructor(private httpClient: HttpClient) {}

  getAllZone(): Observable<any> {
    return this.httpClient.get(`${environment.host}/api/sensor/areas?limit=50`).pipe(tap(async (res: any) => {}));
  }

  getCategories(): Observable<any> {
    return this.httpClient
      .get(`${environment.host}/api/sensor/categories?limit=100`)
      .pipe(tap(async (res: any) => {}));
  }

  getTypes(): Observable<any> {
    return this.httpClient.get(`${environment.host}/api/sensor/types`).pipe(tap(async (res: any) => {}));
  }

  addressLookup(req?: any): Observable<any> {
    const url =
      'https://nominatim.openstreetmap.org/reverse?json_callback=cb&format=json&lat=' +
      req[0] +
      '&lon=' +
      req[1] +
      '&zoom=27&addressdetails=1';
    return this.httpClient.get(url, { responseType: 'text' }).pipe(tap(async (res: any) => {}));
  }

  getMunicipalities(): Observable<any>{
    return this.httpClient.get(`${environment.json_server}/comuni`).pipe(tap(async (res: any) => {}));
  }
}
