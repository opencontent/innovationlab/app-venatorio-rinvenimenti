import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectMunicipalityPageRoutingModule } from './select-municipality-routing.module';

import { SelectMunicipalityPage } from './select-municipality.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, SelectMunicipalityPageRoutingModule],
  declarations: [SelectMunicipalityPage],
})
export class SelectMunicipalityPageModule {}
